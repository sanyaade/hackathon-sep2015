# Dillinger

You need Docker installed in order to run the example:

```sh
$ docker build -t local:televisions .
$ docker run -d --name="tvs" -p 27001:8080 local:televisions
```

to initialise the televisions and to check the app is working, browse to:

http://localhost:27001/init.cfm

This should return a web page displaying 'app successfully initialised'.

To see a list of connected tvs, browse to:

http://localhost:27001/view.cfm

