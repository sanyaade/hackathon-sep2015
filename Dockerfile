FROM java:7

RUN apt-get update --fix-missing

RUN apt-get install -y apache2

RUN a2enmod proxy
RUN a2enmod proxy_http
RUN a2enmod rewrite

RUN apt-get install -y libreoffice-core
RUN apt-get install -y python2.7
RUN apt-get install -y python
RUN apt-get install -y libc6
RUN apt-get install -y libgcc1
RUN apt-get install -y libpython2.7
RUN apt-get install -y libstdc++6
RUN apt-get install -y uno-libs3
RUN apt-get install -y ure
RUN apt-get install -y python
RUN apt-get install -y python-uno
RUN apt-get install -y unoconv
RUN apt-get install -y npm
RUN apt-get install -y nodejs
RUN npm install socket.io@0.9.10 node-static

RUN cd /opt && wget https://s3-eu-west-1.amazonaws.com/test.play.metadev.io/bluedragon.tar.gz && tar xzf bluedragon.tar.gz

ADD apache /etc/apache2/sites-available
ADD app /opt/bluedragon/webapps/openbd
ADD run.sh /opt/run.sh

CMD ["/opt/run.sh"]
